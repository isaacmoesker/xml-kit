#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "pugixml.hpp"
using namespace std;

class Rules
{
  public:
   Rules(string fileName);
   ~Rules();
   // Fix <head> tag issue
   void fixHead();//yes
   // fix <div> tag issue
   void fixDiv();//yes
   // change <p> to <bibl> where appropriate
   void fixBibl();//yes
   //first string is the name of the nod ethat you want gone,
   //secound string is what you want the name to become
   //pugi::xml_node node = doc;   this has to be infront of it and the third value has to be node so we can actually run it on the whole document defult values do not work
   //names are the red brown in the xml file nothing else
//   void findReplaceNodeName(string, string,pugi::xml_node);
   void fixQuote();//yes
   void fixTable();//yes
   void fixList();//yes
   void fixFig();
   void fixNormal();//yes
   void fixItalic();//yes
   void fixBold(); //yes
   void fixMono();//yes
   void fixCitation();//yes
   void fixQuery();//yes
   void fixAll();//yes
   void save();//yes
   string append(string nameUser, int dateYear);
   vector<pugi::xml_node> xPathSearch(string query);
   void xPathSearchReplace(string oldNode, string newNode, bool delAttributes = false);
   //option 0 makes a parent for that node and moves it alon into that node and no other changes you get that node and all of its children into it very base and can be used everywhere
   void makeParent(pugi::xml_node& node, string parentName, int option = 0);
   void makeChild(pugi::xml_node& node, string parentName, string content= "");
   void xPathSearchInsertAttribute(string query, string attr, string value = "");
   void xPathSearchInsertAttribute(string query, string attr, int value);
   void xPathSearchInsertAttribute(pugi::xml_node& query, string attr, string value);
   
  private:
   pugi::xml_document doc;
   ofstream fout;
   int logTag;
   string fileName1;
   int number;
   int figID;
   string nameSpace;
};
