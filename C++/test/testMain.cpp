#ifndef Team5_CPSC3720_testMain_cpp
#define Team5_CPSC3720_testMain_cpp

#include <cppunit/TextTestRunner.h>

#include "3720_xmlParserTest.h"

int main()
{
	CppUnit::TextTestRunner runner;
	runner.addTest(XmlParserTest::suite());
	runner.run();

}//END METHOD main()

#endif //Team5_CPSC3720_testMain_cpp