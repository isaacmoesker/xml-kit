#ifndef Team5_CPSC3720_XmlParserTest_h
#define Team5_CPSC3720_XmlParserTest_h

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <string>
#include <vector>
#include <utility>
#include <ctime>		//used to get the current time
#include <sstream>
#include <fstream>

#include "Rules.h"
#include "pugixml.hpp"

using std::string;
using std::vector;
using std::pair;
using std::ofstream;
using namespace pugi;

class XmlParserTest : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(XmlParserTest);
	//CPPUNIT_TEST(testFixHead);
	//CPPUNIT_TEST(testFixBibl);		//todo need to revise fixBibl. Test failed
	//CPPUNIT_TEST(testFixQuote);
	//CPPUNIT_TEST(testFixTable);		//todo need to revise fixTable. Test failed
	//CPPUNIT_TEST(testFixList);		//todo need to revise fixList. fixList code was changed. Mainly in creating a parent for every item
										//for (unsigned int i = 0; i < items.size(); i += number)
   										//{
	 									//  makeParent(lists[i], "list", 1);
	 									//  xPathSearchInsertAttribute( "list", "type", "ordered");
   										//}
   										
	//CPPUNIT_TEST(testFixFig);			//todo need to revise fixFig. Test failed
	//CPPUNIT_TEST(testFixItalics);		//todo need test files
	//CPPUNIT_TEST(testFixBold);		//todo need test files
	//CPPUNIT_TEST(testFixMono);
	CPPUNIT_TEST_SUITE_END();

	private:
		Rules *rules;
		string improperXML_filename;
		string properXML_filename;
		ofstream fout;
		xml_document generatedXmlParser;
		xml_document properXmlParser;

		bool compareFile(string generatedFileLocation, string properFileLocation);
		vector<xml_node> getXPathNodes(xml_document &currentXML);
		vector<xml_attribute> getNodeAttributes(xml_node &node);
		bool compareNodeNames(vector<xml_node> &generatedNodesVector, vector<xml_node> &properNodesVector);
		string generateLogFilename();

	public:
		void setUp();
		void tearDown();
		void testFixHead();
		void testFixBibl();
		void testFixQuote();
		void testFixTable();
		void testFixList();
		void testFixFig();
		void testFixItalics();
		void testFixBold();
		void testFixMono();
		//void testFixCitation();   //todo implement test
		//void testFixQuery();		//todo implement test

};//END CLASS XmlParserTest

#endif //Team5_CPSC3720_XmlParserTest_h
