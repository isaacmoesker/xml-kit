// Main 

// Includes
#include "Rules.h"
#include <fstream>

using namespace std;

// Main program
// Takes in two strings; the first being the filename, the second being the journal name
int main(int argc, char *argv[])
{
   string fileName = argv[1];
   string journalName = argv[2];
   
   // Run rules different rules depending on journal name
   if(journalName == "DigitalMedievalist") {
      Rules document(fileName);
      document.fixAll();
   }
   else{
      cout << "Invalid Journal Entry." << endl;
   }
   return 0;
}
