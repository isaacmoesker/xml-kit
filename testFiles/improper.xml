<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <author>Heather</author>
      </titleStmt>
      <editionStmt>
        <edition>
          <date>2014-09-20</date>
        </edition>
      </editionStmt>
      <publicationStmt>
        <p>unknown</p>
      </publicationStmt>
      <sourceDesc>
        <p>Converted from a Word document </p>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <appInfo>
        <application xml:id="doxtotei" ident="TEI_fromDOCX" version="2.15.0">
          <label>DOCX to TEI</label>
        </application>
      </appInfo>
    </encodingDesc>
    <revisionDesc>
      <listChange>
        <change>
          <date>2015-02-08T02:47:01Z</date>
          <name>Heather</name>
        </change>
      </listChange>
    </revisionDesc>
  </teiHeader>
  <text>
    <body>
      <div>
        <head>Practice  Document for XML Conversion</head>
        <p rend="Quote">Heather S. Hobma, University of Lethbridge. heather.hobma@gmail.com</p>
        <p rend="Abstract">Abstract: This is a practice document on which you can practice XML conversion based on the specific instructions given to you by your journal. Overall, each journal uses a similar structure with very minor differences (DM has a specific header they use for the colophon, whereas DS does not). Use this document to practice the most common elements required for proper and robust XML conversion (Chandler 2012).</p>
        <p rend="Normal">Keywords: practice, XML, HTML, document conversion, stylesheets, journal incubator</p>
        <div>
          <head>Introduction</head>
          <p rend="Normal">Your first step is to ensure the document structure and heading levels are correctly nested. The stylesheet ultimately grabs these <hi rend="Intense_Emphasis">tei:div</hi> elements and tei:heading tags and creates a (hopefully) properly levelled table of contents. OxGarage will usually take the heading levels from the paragraph styles you&#x2019;ve applied in the original word processing document and properly nest the structure, but sometimes it doesn&#x2019;t (Craig 2007). The easiest way to see if something is missing is to run the transformation, check to see if any major section is missing, check the ToC to see if any headings are missing (usually, if a major section is not where the stylesheet is expecting it&#x2014;like the abstract&#x2014;the body of the document will not show correctly). </p>
        </div>
        <div>
          <head>Heading levels</head>
          <p rend="Normal">The first tei:div element will start right below the tei:body element, beginning with the document title will at heading level one (1) and will surround the entire document body, including the bibliography. </p>
          <p rend="Normal">The abstract and keywords, which are both at heading level two (2), are contained in their own separate tei:div elements, and a new <hi rend="Intense_Emphasis">tei:div</hi> will start at the main body of the document (requiring its own heading level two (2) named Main Body). </p>
          <p rend="Normal">Each subsequent section requires a separate tei:div element to separate it and nest it within the main section, and such take level three (3) headers. </p>
          <p rend="Normal">The tei:div for the main body must be closed at the end of the last paragraph of the main body, and a new tei:div opened for the bibliography. This section requires no <hi rend="Intense_Emphasis">tei:head</hi> tag, as it is defined by a <hi rend="Intense_Emphasis">tei:listBibl</hi> element. Each item in the bibliography needs to be surrounded by its child tag, <hi rend="Intense_Emphasis">tei:bibl</hi>, and an <hi rend="Intense_Emphasis">xml:id</hi>, using the last name(s) of the author(s) and date of publication.</p>
          <p rend="Normal">If you have properly nested your <hi rend="Intense_Emphasis">tei:div</hi> sections, you should have three closing <hi rend="Intense_Emphasis">tei:div</hi> tags at the end of the document, right before the closing <hi rend="Intense_Emphasis">tei:body</hi> element.</p>
        </div>
        <div>
          <head>Block quotations, images, and tables</head>
          <p rend="Normal">Next you are to code the paragraph-level characteristics (Bennett 1995). The most common ones you&#x2019;ll come across are block quotations, images, and tables (although they are less frequent than the first two). </p>
          <div>
            <head>Block quotations</head>
            <p rend="Normal">If you properly labelled the block quotations in the word processing document, they should be easy to find as your work your way through the document:</p>
            <p rend="Block Quote">They may show surrounded in a <hi rend="Intense_Emphasis">&lt;p rend= &#x201C;text body indent&#x201D;&gt;</hi> tag, or something similar. Block quotes, like lists and figures, need to be integrated with the paragraph above it, surrounded in a <hi rend="Intense_Emphasis">tei:quote</hi> tag, and include a <hi rend="Intense_Emphasis">tei:p</hi> tag (so a paragraph inside a quote inside another paragraph). There should then be a paragraph tag BEFORE and AFTER the end quote tag.</p>
          </div>
          <div>
            <head>Figures</head>
            <p rend="Normal">Figures will probably need to be nearly completely recoded to make to any sense; however, they should appear in the correct place in the document. Remember to rename the image files in the SVN to something like figure1 or image1. Figures need a unique xml:id tag, as well as a locational URL, and figure description (caption). Again, the image will be integrated with the previous paragraph. </p>
            <figure>
              <p rend="Figure">Figure : Anniversary salutations from Geoffrey Chaucer!</p>
              <graphic n="1001" width="7.196666666666666cm" height="9.775472222222222cm" url="media/image1.png" rend="inline">
                <desc>chaucer-END.png</desc>
              </graphic>
            </figure>
          </div>
          <div>
            <head>Lists and tables</head>
            <div>
              <head>Lists</head>
              <p rend="Normal">Lists are structured to be part of the preceding paragraph, while tables are structured to be stand-alone paragraphs (Arvanitis 2005). Lists are simply labelled at ordered or unordered, and contain the child element tei:item. An unordered list will take on this form:</p>
              <p rend="List Paragraph">A butcher</p>
              <p rend="List Paragraph">A baker</p>
              <p rend="List Paragraph">A candlestick maker</p>
              <p rend="Normal">An ordered list will take on a numbered form:</p>
              <p rend="List Paragraph">A butcher</p>
              <p rend="List Paragraph">A baker</p>
              <p rend="List Paragraph">A candlestick maker</p>
            </div>
            <div>
              <head>Tables</head>
              <p rend="Normal">Tables can get pretty complex to code, but OxGarage actually handles them quite well. If a table is exceptionally complicated, however, it&#x2019;s not a bad idea to use an image instead. </p>
              <table rend="rules">
                <row>
                  <cell rend="Normal">Race/Skills</cell>
                  <cell rend="Normal">Altmer</cell>
                  <cell rend="Normal">Argonian</cell>
                  <cell rend="Normal">Bosmer</cell>
                  <cell rend="Normal">Breton</cell>
                </row>
                <row>
                  <cell rend="Normal">Smithing</cell>
                  <cell rend="Normal">15</cell>
                  <cell rend="Normal">15</cell>
                  <cell rend="Normal">15</cell>
                  <cell rend="Normal">15</cell>
                </row>
                <row>
                  <cell rend="Normal">Heavy armour</cell>
                  <cell rend="Normal">15</cell>
                  <cell rend="Normal">15</cell>
                  <cell rend="Normal">15</cell>
                  <cell rend="Normal">15</cell>
                </row>
                <row>
                  <cell rend="Normal">Archery</cell>
                  <cell rend="Normal">15</cell>
                  <cell rend="Normal">15</cell>
                  <cell rend="Normal">25</cell>
                  <cell rend="Normal">15</cell>
                </row>
                <row>
                  <cell rend="Normal">Lockpicking</cell>
                  <cell rend="Normal">15</cell>
                  <cell rend="Normal">25</cell>
                  <cell rend="Normal">20</cell>
                  <cell rend="Normal">15</cell>
                </row>
              </table>
            </div>
          </div>
        </div>
        <div>
          <head>Phrase level elements</head>
          <p rend="Normal">Now it&#x2019;s time to cleanup any phrase level elements, like italics, bold, and monotype. </p>
          <div>
            <head>Italics</head>
            <p rend="Normal">This is the point where you will be applying the metadata to identify <hi rend="Emphasis">why</hi> a word something is italicised: is it a title? What level title is it? Is it merely for emphasis? Is it a foreign word or phrase? Titles need to be identified by their level: journal titles and book (monograph) titles are both italicised, but will be identified differently in XML. For instance, the journal New Literary History will have a different level tag than the book <hi rend="Emphasis">Cybertext: Perspectives on Ergodic Literature.</hi></p>
          </div>
          <div>
            <head>Quotations</head>
            <p rend="Normal">Additionally, analytic titles will be surrounded in quotation marks, such as the article &#x201C;On the Margins of Fernando Pessoa's Private Library,&#x201D; and will be distinguished apart from items in &#x201C;scare quotes,&#x201D; direct quotations, or linguistically distinct items. In-line quotations will take a different structure than block quotations. For example, the quote by William Shakespeare: &#x201C;if music be the food of love, play on.&#x201D; </p>
          </div>
          <div>
            <head>Monotype</head>
            <p rend="Normal">Monotype will be applied to any text that represents code and will be surrounded by the tei:code element (Alpers 2001). For example, if I were to indicate how to code an image in XML, the following representation of that coding would need to be in monotype: </p>
            <p rend="Monotype">&lt;figure&gt;<lb/>                  &lt;figDesc&gt;Caption for the figure.&lt;/figDesc&gt;<lb/>&lt;/figure&gt;</p>
          </div>
        </div>
        <div>
          <head>Bibliography</head>
          <p rend="Normal">The most time consuming part of XML cleanup is usually the bibliography, but it is a good idea to work on this first. Each item needs to be separated from each other and assigned an <hi rend="Intense_Emphasis">xml:id</hi>, so readers can link between in-text citations and the bibliography. The entire list stands apart in its own <hi rend="Intense_Emphasis">tei:div</hi>, apart from the rest of the Main Body. It takes the distinct <hi rend="Intense_Emphasis">tei:listBibl</hi> tag, which creates the &#x201C;Works cited&#x201D; header for the ToC, and contains the child tag <hi rend="Intense_Emphasis">tei:bibl</hi> for each bibliographic item. You will use the xml:id reference locations to link the bibliographic information to the in-text citations. Code the bibliography below, and link each item to the randomly placed in-text citations in the rest of the text.</p>
        </div>
        <div>
          <head>Works Cited</head>
          <p rend="Bibl">Alpers, Svetlana. 2001. &#x201C;The Museum as a Way of Seeing.&#x201D; In Exhibiting Cultures the Poetics and Politics of Museum Display. Washington: Smithsonian Inst. Press. 25-32.</p>
          <p rend="Bibl">Arvanitis, Konstantinos. 2005. &#x201C;Museums Outside Walls: Mobile Phones and the Museum in the Everyday.&#x201D; <hi rend="Emphasis">IADIS International Conference Mobile Learning</hi> Leicester, UK: University of Leicester. 251&#x2013;255.</p>
	  <p rend="Bibl">Arvanitis, Konstantinos. 2005. &#x201C;dfadhfjaksdbfkjasdbfkjasnbfo.</p>
          
          <p rend="Bibl">Bennett, Tony. 1995. <hi rend="italic">The Birth of the Museum</hi>. London: Routledge.</p>
          <p rend="Bibl">Chandler, Nathan. 2012. "What&#x2019;s an NFC tag?" <hi rend="Emphasis">HowStuffWorks.com</hi>. Accessed April 24, 2014. <ref target="http://electronics.howstuffworks.com/nfc-tag.htm"><hi rend="Internet_Link_Internet_Link">http://electronics.howstuffworks.com/nfc-tag.htm</hi></ref></p>
          <p rend="Bibl">Craig, R. Bruce. 2007. Introduction to <hi rend="Emphasis">Interpreting Our Heritage</hi>, by Freeman Tilden. Loc 106-453 of 3209. Chapel Hill: University of North Carolina Press. Kindle e-book.</p>
        </div>
      </div>
    </body>
  </text>
</TEI>
