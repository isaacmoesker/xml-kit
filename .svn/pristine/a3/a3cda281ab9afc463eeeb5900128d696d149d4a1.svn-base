<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <text>
    <body>
      <div>
	<div>
	  <p>
	    <ref target="#figure0001">Figure 1</ref>
	    displays the full results of the Dunning’s analysis in a wordcloud that renders graphically the words that are most salient to sentimentality—those over-represented. The visualisation shows the Dunning results of comparing the training set of sentimental chapters to the testbed that represents the benchmark of average novelistic discourse. It includes all the stop words and demonstrates exactly how strikingly over-represented the word she is, as it dominates the visual field.
	    <figure xml:id="figure0001">
	      <figDesc>
		Visualisation of Words Over-Represented in Sentimental Texts (Including Stopwords)
	      </figDesc>
	      <graphic url="Figures/Figure1.jpg"/>
	    </figure>
	    In
	    <ref target="#figure0002">Figure 2</ref>
	    , which also represents a comparison of the training set to the testbed, I removed the stopwords. In the visualisations, we can see that words involving relationships: (mother, child, father) and emotion words (love, heart, sob, tear) really stand out.  There are some interesting tensions in the visualisation: between pain and comfort, life and death, happiness and agony, hope and despair.  The sentimental, we see, is played out in extremes. The visualisation also features several pairs of synonymous words or words with alternate spellings, especially the familial words: “baby” and “babby,” “papa” and “father,” “mother” and “mama.” Here we see a tension between the desire for the formal, elevated style that befits the “high” drama of a sentimental moment, and the inclination to want to capture intimacy through less formal and more colloquial language.
	    <figure xml:id="figure0002">
	      <figDesc>
		Visualisation of Words Over-Represented in Sentimental Texts
	      </figDesc>
	      <graphic url="Figures/Figure2.jpg"/>
	    </figure>
	  </p>
	</div>
      </div>
    </body>
  </text>
</TEI>
