<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <text>
    <body>
      <div>
	<div>
	  <div>
	    <listBibl>
	      
	      <bibl xml:id="Adam2005">
		Adam, Julie. 2005.
		<title level="a">
		  The Implicated Audience: Judith Thompson’s Anti-Naturalism in
		  <title level="m">The Crackwalker</title>
		  ,
		  <title level="m">White Biting Dog</title>
		  ,
		  <title level="m">I am Yours</title>
		  , and
		  <title level="m">Lion in the Streets</title>
		  .
		</title>
		<title level="m">
		  Critical Perspectives on Canadian Theatre in English Volume Three
		</title>
		. Ric Knowles ed., 41-46. Toronto: Playwrights Canada Press.
	      </bibl>
	      <bibl xml:id="BestArneilHolmes2002">
		Best, Michael, Stuart Arneil, and Martin Holmes. 2002.
		<title level="m">Scenario</title>
		. Half-Baked Software. University of Victoria Computer-Aided Language Learning Laboratory, CD-ROM.
	      </bibl>
	      <bibl xml:id="BloorStreetBridge1920">
		<title level="a">Bloor Street Bridge in 1920.</title>
		1920. Photograph. City of Toronto Archives, Fonds 1244, Item 2473.
	      </bibl>
	      <bibl xml:id="BloorStreetBridge1984a">
		<title level="a">Bloor Street Bridge in 1984, Side View.</title>
		1984a. Photograph. City of Toronto Archives, Fonds 268, File 7, Item 11.
	      </bibl>
	      <bibl xml:id="BloorStreetBridge1984b">
		<title level="a">Bloor Street Bridge in 1984, Street View.</title>
		1984b. Photograph. City of Toronto Archives, Fonds 268, File 6, Item 35.
	      </bibl>
	      <bibl xml:id="Cushman2011">
		Cushman, Robert. 2011.
		<title level="a">
		  Theatre Reviews: Savage Whimsy in White Biting Dog and Cirque’s Totem.
		</title>
		<title level="m">National Post</title>
		. 26 August.
		<ref target="http://arts.nationalpost.com/2011/08/26/">http://arts.nationalpost.com/2011/08/26/.</ref>
	      </bibl>
	      <bibl xml:id="DeSouzaCoelho2012">
		DeSouza-Coelho, Shawn. 2012.
		<title level="m">Bloor Street Viaduct Street View</title>
		. Toronto, Canada. Photograph.
	      </bibl>
	      <bibl xml:id="Dessen2010">
		Dessen, Alan. 2010.
		<title level="a">
		  The Elizabethan Script-to-Stage Process: The Playwright, Theatrical Intentions, and Collaboration.
		</title>
		<title level="m">Style</title>
		44.3: 391-403.
	      </bibl>
	      <bibl xml:id="Dondis1973">
		Dondis, Donis A. 1973.
		<title level="m">A Primer of Visual Literacy</title>
		. Cambridge, MA: MIT Press.
	      </bibl>
	      <bibl xml:id="Elam1980">
		Elam, Keir. 1980.
		<title level="m">The Semiotics of Theatre and Drama</title>
		. London: Methuen.
	      </bibl>
	      <bibl xml:id="GandyRobertsonPriceBailey2005">
		Gandy, Meredith, Scott Robertson, Ed Price, and Joseph M. Bailey. 2005.
		<title level="a">
		  The Design of a Performance Simulation System for Virtual Reality.
		</title>
		<title level="m">
		  Proceedings of Human-Computer Interaction International
		</title>
		, July 22-27, Las Vegas, NV.
	      </bibl>
	      <bibl xml:id="GriffinCockett2011">
		Griffin, Andrew and Peter Cockett, eds. 2011.
		<title level="m">King Leir</title>
		. Queen’s Men Editions. Internet Shakespeare Editions, University of Victoria, 24 January 2012.
		<ref target="http://qme.internetshakespeare.uvic.ca/Library/Texts/Leir/intro/Performance/default/">
		  http://qme.internetshakespeare.uvic.ca/Library/Texts/Leir/intro/Performance/default/
		</ref>
		.
	      </bibl>
	      <bibl xml:id="Glassco1984">
		Glassco, Bill (director). 1984.
		<title level="m">White Biting Dog</title>
		by Judith Thompson. Archival video. Tarragon Theatre Collection. VHS.
	      </bibl>
	      <bibl xml:id="Hildy2008">
		Hildy, Franklin J. 2008.
		<title level="a">
		  ‘The Essence of Globeness’: Authenticity, and the Search for Shakespeare’s Stagecraft.
		</title>
		<title level="m">Shakespeare’s Globe</title>
		. Christie Carson and Farah Karim-Cooper eds., 13-25. Cambridge: Cambridge University Press.
	      </bibl>
	      <bibl xml:id="Hildy1990">
		---. 1990.
		<title level="a">Reconstructing Shakespeare’s Theatre.</title>
		<title level="m">
		  New Issues in the Reconstruction of Shakespeare’s Theatre: Proceedings of the Conference Held at the University of Georgia Febrary 16-18
		</title>
		. Franklin J. Hildy ed., 1-37. New York: Peter Lang.
	      </bibl>
	      <bibl xml:id="Hildy2004">
		---. 2004.
		<title level="a">Why Elizabethan Spaces?</title>
		<title level="j">
		  Elizabethan Performances in North American Spaces: Theatre Symposium
		</title>
		12: 98-120.
	      </bibl>
	      <bibl xml:id="KVL2002">
		King’s Visualization Lab (KVL). 2002.
		<title level="m">Theatron</title>
		. King’s Visualization Lab, King’s College London. 11 February 2012.
		<ref target="http://www.theatron.org/">http://www.theatron.org/</ref>
		.
	      </bibl>
	      <bibl xml:id="KVL2008">
		---. 2008.
		<title level="a">Publicity.</title>
		King’s Visualization Lab. King’s College London. 13 February 2012.
		<ref target="http://www.kvl.cch.kcl.ac.uk/publicity.html">http://www.kvl.cch.kcl.ac.uk/publicity.html</ref>
		.
	      </bibl>
	      <bibl xml:id="KVL2009a">
		---.2009a.
		<title level="a">THEATRON Final Report.</title>
		<title level="m">
		  Theatron3 - Educational undertakings in Second Life. English Subject Centre, The Higher Education Academy, Council for College and University Education
		</title>
		. 23 January 2012.
		<ref target="http://www.english.heacademy.ac.uk/explore/projects/archive/technology/tech23.php">
		  http://www.english.heacademy.ac.uk/explore/projects/archive/technology/tech23.php
		</ref>
		.
	      </bibl>
	      <bibl xml:id="KVL2009b">
		---. 2009b.
		<title level="m">Theatron3</title>
		. King’s Visualization Lab, King’s College London. 10 February 2012.
		<ref target="http://cms.cch.kcl.ac.uk/theatron/">http://cms.cch.kcl.ac.uk/theatron/</ref>
		.
	      </bibl>
	      <bibl xml:id="KVLnd">
		---. N.d.
		<title level="a">Theatron3.</title>
		<title level="m">Second Life</title>
		.
		<ref target="http://secondlife.com/">http://secondlife.com/</ref>
		.
	      </bibl>
	      <bibl xml:id="Knowles1994">
		Knowles, Ric. 1994.
		<title level="m">Reading the Material Theatre</title>
		. Cambridge: Cambridge University Press.
	      </bibl>
	      <bibl xml:id="LePage1984">
		LePage, Sue (designer). 1984. Technical Drawings for
		<title level="m">White Biting Dog</title>
		. Tarragon Theatre, Toronto. L.W. Conolly Theatre Archives, Guelph, Ontario.
	      </bibl>
	      <bibl xml:id="Levin2005">
		Levin, Laura. 2005.
		<title level="a">
		  Environmental Affinities: Naturalism and the Porous Body.
		</title>
		<title level="m">
		  Critical Perspectives on Canadian Theatre in English
		</title>
		Vol. 3. Ric Knowles ed., 123-135. Toronto: Playwrights Canada Press.
	      </bibl>
	      <bibl xml:id="Lopez2008">
		Lopez, Jeremy. 2008.
		<title level="a">A Partial Theory of Original Practice.</title>
		<title level="j">Shakespeare Survey</title>
		61: 302-317.
	      </bibl>
	      <bibl xml:id="MacDormanGreenHoKoch2009">
		MacDorman, Karl F., Robet Green, Chin-Chang Ho, and Clinton T. Koch. 2009.
		<title level="a">
		  Too Real For Comfort? Uncanny responses to computer generated faces.
		</title>
		<title level="j">Computers in Human Behaviour</title>
		25.3: 695-710.
	      </bibl>
	      <bibl xml:id="Maufort1997">
		Maufort, Marc. 1997.
		<title level="a">
		  Exploring the Other Side of the Dark: Judith Thompson’s Magic Realism.
		</title>
		<title level="m">
		  ‘Union in Partition’: Essays in Honour of Jeanne Delbaere
		</title>
		. Gilbert Debusscher and Marc Maufort eds., 191-200. Liege: L3-Liege Language and Literature.
	      </bibl>
	      <bibl xml:id="Marshall1984">
		Marshall, Raymond. 1984. Prompt script for the production of
		<emph>White Biting Dog</emph>
		by Judith Thompson. Tarragon Theatre: Toronto. L.W. Conolly Theatre Archives, Guelph, Ontario.
	      </bibl>
	      <bibl xml:id="McConachie2010">
		McConachie, Bruce. 2010.
		<title level="a">Reenacting Events to Narrate Theatre History.</title>
		<title level="m">
		  Representing the Past: Essays in Performance Historiography
		</title>
		. Charlotte M. Canning and Thomas Postlewait, eds., 194-214. Iowa City: University of Iowa Press.
	      </bibl>
	      <bibl xml:id="Nunn2005">
		Nunn, Robert. 2005.
		<title level="a">Spatial Metaphors in the Plays of Judith Thompson.</title>
		<emph>
		  Judith Thompson: Critical Perspectives in Canadian Theatre in English
		</emph>
		. Vol. 3. Ric Knowles, ed., 20-40. Toronto: Playwrights Canada Press.
	      </bibl>
	      <bibl xml:id="Ortelia2012">
		Ortelia. 2012.
		<title level="m">Ortelia Interactive Spaces</title>
		. 23 January 2012.
		<ref target="www.ortelia.com">www.ortelia.com</ref>
		.
	      </bibl>
	      <bibl xml:id="Palk2011">
		Palk, Nancy (director). 2011.
		<title level="m">White Biting Dog</title>
		by Judith Thompson. Toronto Soulpepper Theatre. Performance: Theatre. 15 August 2011.
	      </bibl>
	      <bibl xml:id="Postlewait1991">
		Postlewait, Thomas. 1991.
		<title level="a">
		  Historiography and the Theatrical Event: A Primer with Twelve Cruxes.
		</title>
		<title level="j">Theatre Journal</title>
		43.2: 157-178.
	      </bibl>
	      <bibl xml:id="Read2011">
		Read, Kelly. 2011. Technical Drawings for
		<title level="m">White Biting Dog</title>
		, 2011. Soulpepper Theatre, Toronto.
	      </bibl>
	      <bibl xml:id="Roberts-SmithDobsonGabrieleRueckerSinclairBouchardDeSouzaCoelhoJakackiAkongLamRodriguezArenaForthcoming">
		Roberts-Smith, Jennifer, Teresa Dobson, Sandra Gabriele, Stan Ruecker, and Stéfan Sinclair with Matt Bouchard, Shawn DeSouza-Coelho, Diane Jakacki, Annemarie Akong, David Lam, and Omar Rodriguez-Arenas. 2013 (Forthcoming).
		<title level="a">
		  Visualizing Theatrical Text: From
		  <title level="m">Watching the Script</title>
		  to the
		  <title level="m">Simulated Environment for Theatre (SET).</title>
		</title>
		<title level="j">Digital Humanities Quarterly</title>
		.
	      </bibl>
	      <bibl xml:id="RueckerGrotkowskiGabrieleRobertsSmithSinclairDobsonAkongFungHongDeSouzaCoelhoRodriguez2013">
		Stan Ruecker, Ali Grotkowski, Sandra Gabriele, Jennifer Roberts-Smith, Stéfan Sinclair, Teresa Dobson, Annemarie Akong, Sally Fung, Marcelo Hong, Shawn DeSouza-Coelho, and Omar Rodriguez. 2013 (Forthcoming).
		<title level="a">
		  Abstraction and Realism in the Design of Avatars for the Simulated Environment for Theatre.
		</title>
		<title level="j">Visual Communication</title>
		12.4.
	      </bibl>
	      <bibl xml:id="Saltz2004a">
		Saltz, David Z. 2004a.
		<title level="a">Virtual Vaudeville: The Concept.</title>
		<title level="m">Virtual Vaudeville</title>
		. University of Georgia. 20 January 2012.
		<ref target="http://vvaudeville.drama.uga.edu/index.htm">http://vvaudeville.drama.uga.edu/index.htm</ref>
		.
	      </bibl>
	      <bibl xml:id="Saltz2004b">
		---. 2004b.
		<title level="a">In Development.</title>
		<title level="m">Virtual Vaudeville</title>
		. University of Georgia. 20 January 2012.
		<ref target="http://vvaudeville.drama.uga.edu/index.htm">http://vvaudeville.drama.uga.edu/index.htm</ref>
		.
	      </bibl>
	      <bibl xml:id="Sarlós1979">
		Sarlós, Robert K. 1979.
		<title level="a">
		  Creating Objects and Events: A Form of Theatre Research.
		</title>
		<title level="j">Theatre Research International</title>
		5: 83-88.
	      </bibl>
	      <bibl xml:id="SoulpepperTheatreCompany2011a">
		Soulpepper Theatre Company. 2011a.
		<title level="a">White Biting Dog - Artist Profile.</title>
		<title level="m">Soulpepper</title>
		. 29 September 2011.
	      </bibl>
	      <bibl xml:id="SoulpepperTheatreCompany2011b">
		Soulpepper Theatre Company. 2011b.
		<title level="a">The Company/The Soulpepper Difference.</title>
		<title level="m">Soulpepper</title>
		. 26 October 2011.
	      </bibl>
	      <bibl xml:id="Thompson1985">
		Thompson, Judith. 1985.
		<title level="m">White Biting Dog</title>
		. Toronto: Playwrights Canada Press.
	      </bibl>
	      <bibl xml:id="TompkinsDelbridge2009">
		Tompkins, Joanne, and Matthew Delbridge. 2009.
		<title level="a">
		  Using Virtual Reality Modelling In Cultural Management, Archiving And Research.
		</title>
		<title level="m">
		  EVA London 2009: Electronic Visualisation and the Arts
		</title>
		. Conference Proceedings. Alan Seal, Suzanne Keene, Jonathan Bowen, eds. London: British Computing Society. July. 260-269.
	      </bibl>
	      <bibl xml:id="VonTiedemann2011">
		Von Tiedemann, Cylla (photographer). 2011.
		<title level="m">Mike Ross &amp; Michaela Washburn in White Biting Dog</title>
		. Digital photograph. Photo Gallery of the Soulpepper Theatre Company. 26 October.
	      </bibl>
	      <bibl xml:id="White.n.d.a">
		White, Martin. n.d.a.
		<title level="a">
		  Artificial Lighting in the English Commercial Playhouse.
		</title>
		<title level="m">
		  The Chamber of Demonstrations: Reconstructing the Jacobean Indoor Playhouse
		</title>
		. Bristol University. 10 February 2012.
		<ref target="http://www.bristol.ac.uk/drama/jacobean/">http://www.bristol.ac.uk/drama/jacobean/</ref>
		.
	      </bibl>
	      <bibl xml:id="White.n.d.b">
		---. N.d.b.
		<title level="a">Practicing Theatre History as Performance.</title>
		<title level="m">
		  The Chamber of Demonstrations: Reconstructing the Jacobean Indoor Playhouse
		</title>
		. Bristol University. 10 February 2012.
		<ref target="http://www.bristol.ac.uk/drama/jacobean/">http://www.bristol.ac.uk/drama/jacobean/</ref>
		.
	      </bibl>
	      <bibl xml:id="Worthen2010">
		Worthen, William. 2010.
		<title level="m">Drama: Between Poetry and Performance</title>
		. Malden, MA: Wiley-Blackwell.
	      </bibl>
	    </listBibl>
	  </div>
	</div>
      </div>
    </body>
  </text>
</TEI>
