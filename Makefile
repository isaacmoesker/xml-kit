OBJS = C++/main.o C++/Rules.o pugixml-1.5/src/pugixml.o
CPP = g++
CFLAGS = -Wall -g -I pugixml-1.5/src/ -I C++/

all : Main clean run

Main : $(OBJS)
	$(CPP) $(CFLAGS) $^ -o $@
%.o : %.cpp
	$(CPP) -c $(CFLAGS) $< -o $@

clean: 
	rm -f *.o *~ *% *# .#*
	rm -f C++/*.o *~ *% *# .#*
	rm -f pugixml-1.5/src/*.o *~ *% *# .#*

run:
	./Main testFiles/improper.xml "DigitalMedievalist"

.PHONY : clean-all
clean-all : clean
		rm -f Main
