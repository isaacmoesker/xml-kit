XML Kit User Manual
2015 Edition

**Getting Started**

* The program is run through a PHP application that executes the C++ underlay. This means that the program will need to be hosted on a web server in order to make full use of the capabilities it possesses.
* Various web servers are available online, and this will allow multiple users to use the software at one time and in many different locations without using computer storage or having to deal with applications.

**Program Chronology**

The program handles the cases brought up in the XML instructions manual (attached at the back). In detail, the following are handled (in order of handling):

1. XML file upload
2. <p rend=’Heading_1’> tags changed to <head>
3. <p rend=’Heading_2’> tags changed to <head>
4. <p rend=’Bibl’> tags changed to <bibl>
5. <bibl> ID’s assigned using the Author and Year
6. <listbibl> tags added around the <bibl> tags
7. <listbibl> section moved to the bottom <div> tag
8. <title> tags added to the first level of the <bibl> text
9. <title> level ‘a’ attribute added to the <title> tags
10. <italic> tags changed to <title> in the <bibl>
11. <title> level ‘m’ added to the <bibl>
12. <p rend=’Quote’> tags changed to <quote>
13. <p> tags added underneath <quote> tags
14. <p rend=’Block Quote’> tags changed to <quote>
15. <p> tags added underneath <quote> tags
16. <table> tag attributes “rend=rules” added
17. <p rend=’List Paragraph’> tags changed to <item>
18. <list> tags surround <item> sets
19. <list> attributes order added
20. <figure> tag ID’s added
21. <figDesc> tags added to the <figure>
22. <hi rend=’Emphasis’> tags replaced with <italic> tags
23. <hi rend=’Intense Emphasis’> tags replaced with <bold>
24. <p rend=’Monotype’> tags replaced with <code> tags
25. XSLT Processor running
26. XML file downloaded and stored with the original name + _fixed
27. Text file outputted stating what has happened

**Self Tagging** (Required By Program To Handle Properly)

1. If none of the title level tags want to be hand changed (depending on level ID) tag them with the following:
a. <title level=’a’> → Ta
b. <title level=’j> → Tj
c. <title level=’m’> → Tm
2. All bibliography items must be tagged with “Bibl” so that the program can distinguish them from regular paragraphs

**XML Instructions**

*Clean up the XML*

It is now time to clean up the XML. This is the major part of your pre­proofing work on the document. You can expect to make many passes through the document and, because every document is unique, there is no set algorithm to follow. In broad terms, however, these are the main things you will need to do:

*Make sure the heading structure of the document has been converted properly*

1. OxGarage sometimes is able to create a properly structured XML document from the implicit structure of the “Heading” styles you applied earlier. Other times it is not able.
2. If it has been done properly, the word processing document headings will have been converted to tei:head elements, each of which will appear as the first item in a tei:div element. The hierarchy will be reflected by the level of nesting involved in the divisions. The document title will appear as a head with only a single div element above it; the second level heading will appear as a head inside two divisions, and so on.
3. If it hasn’t done it properly, it will have converted the section headings to tei:p elements, with the word process style name as the value of a rend attribute (e.g. <p ren=”Heading_1”>). This means you will have to apply the section nesting by hand.
a. Find the highest numbered heading in the document (i.e. the heading for the most minor section).
b. Highlight this entire section (including any associated text paragraphs, tables, and the like).
c. Surround this section with a tei:div tag.
4. Repeat the process with the next highest numbered headings in the document (i.e. the headings for the next most minor sections), working further up the document hierarchy until you reach the final section, headed by the first header or document title.

**The Bibliography**

1. Find the works cited list.
2. Highlight all the entries in the list.
3. Surround the bibliography with a tei:listBibl.
4. You will get an error at this point, because Oxgarage has converted all the bibliography enteries to paragraphs, and listBibl can only have tei:bibl as its child. You will now need to convert all the bibliographic paragraphs to "bibl." If you followed the advice above and applied a unique style to these paragraphs, you can do this programmatically either using the search and replace function or a stylesheet. If you didn't, you will have to change the tags by hand.
5. Give all the bibliographic items an xml:id attribute.
6. Later, you will be linking to these works cited references from the main text. So it makes sense to provide ID numbers now. I do this by copying the author names, adding an xml:id attribute to bibl, pasting the author names between the quotes, and then removing everything but the author last names. I finish the xml:id with the item date (e.g. an author list like this "Brown, Susan, Richard Cunningham, and James Cummings" for a book published in 2003 will end up like xml:id="BrownCunninghamCummings2003."

**Code Block Quotes**

If the original document had block quotations, you should look for these now. If you used styles as I recommended above, you will find them as <p rend="Text_Body_Indent"> or similar.

Block quotes, like lists and pictures, need to be integrated with the paragraph above it. So first, delete the </p> of the preceding paragraph and the opening <p> of the block
quote paragraph.

Then, highlight the entire text that is in the block quote, and type control+e in order to get the pop­up box. Start typing ‘quote’ to surround the entire block quote with the tag <quote>.

Once that's there, highlight the exact same text (inside the tei:quote) and surround it with the <p> tag, so that the final product looks like this:
<p>Above paragraph introducing block quote:
<quote><p>This is the text of the block quote</p></quote></p>

Notice that there will be a </p> tag both BEFORE and AFTER the end quote tag. This is because the quote is merged with the paragraph preceding it, so the paragraph officially ends after the block quote. The <p> tags INSIDE the block quote indicate that it should be set apart from the rest of the paragraph: in other words, a paragraph within a paragraph.

**Structure any lists and tables**

Lists are formatted to be part of the preceding paragraph. And tables will be formatted as stand­alone paragraphs. In most TEI documents, they should more properly appear in a paragraph. Follow the second set of instructions above for joining block quotations to paragraphs (you won't need to surround the lists or tables with any tags, just join them to the following or preceding paragraphs). Not all tables and paragraphs are associated logically with the preceding or following paragraph. In such cases just leave them where they are!

<list type="ordered"> or <list type=“unordered”>
<item>a butcher</item>
<item>a baker</item>
<item>a candlestick maker</item>
</list>

Tables represent a sizable challenge for any text processing system, but simple tables, at least, appear in so many texts that even in the simplified TEI tag set presented here, markup for tables is necessary. The following elements are provided for this purpose:

<table> contains text displayed in tabular form, in rows and columns. A rend=“rules” attribute in will box­in the table. Other attributes include: rows, which indicates the number of rows in the table; cols: indicates the number of columns in each row of the table.

Child tags include:
* <row> contains one row of a table. Attributes include: role, which indicates the kind of information held in the cells of this row. Suggested values include label for labels or descriptive information, and data for actual data values.
* <cell> contains one cell of a table. Attributes include: role, which indicates the kind of information held in the cell. Suggested values include label for labels or descriptive information, and data for actual data values; cols, which indicates the number of columns occupied by this cell; rows, which indicates the number of rows occupied by this cell.

**Structure any figures**

1. If there are images or figures in the document, you will need to format them. There are three parts to a figure in the TEI XML we use, but OxGarage does not always put them together or supply all parts: a tei:figure (<figure>) element for the container; a tei:graphic (<graphic url=“”) element that contains a link to the image file; and a tei:figDesc (<figDesc>) element (almost certainly missing) that contains the caption.
2. At the location you want the image to appear, put in a tei:figure element. As with tables, block quotations, and lists, this can be either within or between paragraphs.
3. Inside the tei:figure element, add a tei:figDesc element. Add the text you want to use as the caption (without any preceding "Figure 1" or similar­type text, as this will be added automatically). If there were captions in the original document, these may appear as <p> somewhere nearby.
4. Add the tei:graphic element inside tei:figure. You should make sure the tei:graphic element is empty (i.e. ends in />). OxGarage by default adds a description text (i.e. looks something like this: >Description here...<graphic>).
5. The URL attribute on an image should point to the image file name. 

A properly formatted figure will look like this:
<figure xml:id="figure0001">
<figDesc>Caption for the figure.</figDesc>
<graphic url="figures/Figure1.jpg"/>
</figure>

**Cleanup phrase level elements like italics and bold**

The character styles we applied in the word processor document only indicate at this point that the text in question was highlighted in some way (using tei:hi ­ <hi>) with a rend attribute explaining what it looked like. We now need to provide metadata explaining why the elements are the way they are.

1. The biggest one to deal with will be <hi rend="Emphasis">: most of these will probably actually reflect book or journal titles (in which case you should replace them with <title level="m"> or <title level="j"> elements).
2. Some may represent foreign words or phrases (in which case use <foreign xml:lang=""> (with a two or three letter language code on xml:lang to indicate which language: eng = English; fra = French; deu= German; lat=Latin).
3. Other purposes for the highlighting might include emphasis (replace with <emph>), to indicate a technical term being discussed (in which case <term>). If you don't know, use <hi rend="italic">.
4. Do the same for things marked Strong Emphasis. 

**Cleanup monotype phrases**

These are usually replaced by the tei:code element.

**Convert all citations in the text to references pointing at the relevant works cited item**

1. Surround the citation text with tei:ref.
2. Add a target attribute.
3. From the drop down list that appears, select the relevant bibliographic item.

**Including proofing queries for the author(s)**

If you have any queries for the author, indicate these as follows:

1. Surround the text to which your query pertains with a tei:hi element; set the rend element to "proofing."
2. Immediately after the closing tag of this hi element, insert a seg@rend="proofing" element.
3. Type your query into the tei:seg element. These will be collected at the end of the document by the HTML stylesheet.

**Run the final, automatic conversions**

This adds id numbers to the paragraph elements, etc. You do this by running the danDSCN stylesheet for DS/CN, and the similar in DM, in Oxygen. Once the transformation is complete, review the HTML output.

When the proofing process is over and the document is ready to move into final publication, remove the n="proofing" attribute from the root element.