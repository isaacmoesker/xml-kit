<?php

include "xmlupload.php";
include "styleupload.php";

class TestUpload extends PHPUnit_Framework_TestCase
{
    public function testXMLCheckFileType()
    {
        $upload = new XMLUpload();
        
        // Test return false
		$pass = $upload->CheckFileType("xml");
		$this->assertEquals(true, $pass);
		
		// Test return true, wrong file type
		$pass = $upload->CheckFileType("jpg");
		$this->assertEquals(false, $pass);
		
		// Test return true, no file type
		$pass = $upload->CheckFileType("");
		$this->assertEquals(false, $pass);
    }
    
    // SaveFile() cannot be tested
    
    public function testXMLExecute() {
    	$upload = new XMLUpload();
    	
    	$fileBaseName = $upload->Execute("improper.xml", "DigitalMedievalist");
    	$this->assertEquals("improper_fixed.xml", $fileBaseName);
    }
    
    // DownloadFiles() cannot be tested
    
    public function testXSLCheckFileType()
    {
        $upload = new XSLUpload();
        
        // Test return false
		$pass = $upload->CheckFileType("xsl", "xml");
		$this->assertEquals(true, $pass);
		
		// Test return true, wrong file type
		$pass = $upload->CheckFileType("jpg", "xml");
		$this->assertEquals(false, $pass);
		
		// Test return true, wrong file type
		$pass = $upload->CheckFileType("xml", "jpg");
		$this->assertEquals(false, $pass);
		
		// Test return true, no file type
		$pass = $upload->CheckFileType("", "");
		$this->assertEquals(false, $pass);
    }
    
    // SaveFile() cannot be tested
    
    public function testXSLExecute() {
    	$upload = new XSLUpload();
    	
    	$fileBaseName = $upload->Execute("test.xsl", "test.xml");
    	$this->assertEquals("test_styled.html", $fileBaseName);
    }
    
    // DownloadFiles() cannot be tested
}

?>
