<?php

if(isset($_POST["submit"])) {
	$fileBaseName = basename($_FILES["fileToUpload"]["name"]);
	$fileTempName = $_FILES["fileToUpload"]["tmp_name"];
	$journalName = $_POST["journalName"];
	$fileType = pathinfo($fileBaseName, PATHINFO_EXTENSION);
	$upload = new XMLUpload();
	
	if($upload->CheckFileType($fileType)) {
		if($upload->SaveFile($fileTempName, $fileBaseName)) {
			$fileNewName = $upload->Execute($fileBaseName, $journalName);
			$upload->DownloadFiles($fileBaseName, $fileNewName);
		}
	}
}

class XMLUpload {
	public function __construct() {
	}

	public function CheckFileType($fileType) {
		// Only allow XML file format
		if($fileType == "xml") {
			return true;
		}
		else {
			echo "Sorry, only XML files are allowed.";
			return false;
		}
	}
	
	public function SaveFile($fileTempName, $fileBaseName) {
		if (move_uploaded_file($fileTempName, $fileBaseName)) {
			return true;
		}
		else {
			echo "Sorry, there was an error uploading your file.";
			return false;
		}
	}

	public function Execute($fileBaseName, $journalName) {
		// if everything is ok, try to upload file
		// call C++ Main
		exec("./Main $fileBaseName $journalName");
		// rename log of all operations in C++ program
		rename("improper.txt", "log.txt");
		
		$length = strlen($fileBaseName);
		$fileBaseName = substr($fileBaseName, 0, $length - 4) . '_fixed.xml';
		return $fileBaseName;
	}
	
	public function DownloadFiles($fileBaseName, $fileNewName) {
		$zipname = "files.zip";
		$file_names = array("log.txt", $fileNewName);
		$zip = new ZipArchive();
		
		//create the file and throw the error if unsuccessful
		if ($zip->open($zipname, ZIPARCHIVE::CREATE )!==TRUE) {
        	exit("cannot open <$zipname>\n");
    	}
		
		//add each files of $file_name array to archive
		foreach($file_names as $files) {
			$zip->addFile($file_path.$files,$files);
		}
		$zip->close();
		
		header("Content-type: application/zip"); 
		header("Content-Disposition: attachment; filename=$zipname"); 
		header("Pragma: no-cache"); 
		header("Expires: 0");
		readfile($zipname);
		
		// delete files from server
		unlink($fileBaseName);
		unlink($fileNewName);
		unlink("log.txt");
		unlink($zipname);
	}
}
?>
