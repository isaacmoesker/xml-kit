<?php

if(isset($_POST["submit"])) {
	$styleFileBaseName = basename($_FILES["styleFile"]["name"]);
	$xmlFileBaseName = basename($_FILES["xmlFile"]["name"]);
	$styleFileTempName = $_FILES["styleFile"]["tmp_name"];
	$xmlFileTempName = $_FILES["xmlFile"]["tmp_name"];
	$styleFileType = pathinfo($styleFileBaseName, PATHINFO_EXTENSION);
	$xmlFileType = pathinfo($xmlFileBaseName, PATHINFO_EXTENSION);
	$upload = new XSLUpload();
	
	if($upload->CheckFileType($styleFileType, $xmlFileType)) {
		if($upload->SaveFile($styleFileTempName, $styleFileBaseName, $xmlFileTempName, $xmlFileBaseName)) {
			$xmlFileNewName = $upload->Execute($styleFileBaseName, $xmlFileBaseName);
			$upload->DownloadFiles($styleFileBaseName, $xmlFileNewName, $xmlFileBaseName);
		}
	}
}

class XSLUpload {
	public function __construct() {
	}

	public function CheckFileType($styleFileType, $xmlFileType) {
		// Only allow XML file format
		if($styleFileType == "xsl" && $xmlFileType == "xml") {
			return true;
		}
		else {
			echo "Sorry, only XML files are allowed.";
			return false;
		}
	}
	
	public function SaveFile($styleFileTempName, $styleFileBaseName, $xmlFileTempName, $xmlFileBaseName) {
		if (move_uploaded_file($styleFileTempName, $styleFileBaseName)
			&& move_uploaded_file($xmlFileTempName, $xmlFileBaseName)) {
			return true;
		}
		else {
			echo "Sorry, there was an error uploading your file.";
			return false;
		}
	}

	public function Execute($styleFileBaseName, $xmlFileBaseName) {
		// Load files 
		$xml = new DOMDocument();
		$xml->load($xmlFileBaseName);
		$xsl = new DOMDocument();
		$xsl->load($styleFileBaseName); 

		// Start XSLT
		$proc = new XSLTProcessor();
		$proc->importStylesheet($xsl);  
		$html = $proc->transformToXML($xml);
		
		// Generate name
		$length = strlen($xmlFileBaseName);
		$xmlFileNewName = substr($xmlFileBaseName, 0, $length - 4) . '_styled.html';
		
		// Write html to new html document
		$newFile = fopen($xmlFileNewName, "w") or die("Unable to open file!");
		fwrite($newFile, $html);
		fclose($newFile);

		//rename($xmlFileBaseName, $xmlFileNewName);
		return $xmlFileNewName;
	}
	
	public function DownloadFiles($styleFileBaseName, $xmlFileNewName, $xmlFileBaseName) {
		header("Content-type: text/html"); 
		header("Content-Disposition: attachment; filename=$xmlFileNewName"); 
		header("Pragma: no-cache"); 
		header("Expires: 0");
		readfile($xmlFileNewName); // download the file
		
		// delete files from server
		unlink($xmlFileNewName);
		unlink($styleFileBaseName);
		unlink($xmlFileBaseName);
	}
}
?>
